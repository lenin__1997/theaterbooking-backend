package com.codeboard.theatrebooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.codeboard.theatrebooking")
public class TheatrebookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheatrebookingApplication.class, args);
	}

}
