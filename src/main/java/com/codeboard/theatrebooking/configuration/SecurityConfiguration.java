//package com.codeboard.theatrebooking.configuration;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
//
//import com.codeboard.theatrebooking.service.UserServiceImple;
//
////@EnableWebSecurity
////@Configuration
//public class SecurityConfiguration {
//
//	@Autowired
//	private UserServiceImple userDetailsService;
//
//	@Bean
//	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//		return http.csrf().disable()
//				.exceptionHandling(request -> request.authenticationEntryPoint(new BasicAuthenticationEntryPoint()))
//				.authorizeHttpRequests(
//						(requests) -> requests.requestMatchers("/userlogin").permitAll().anyRequest().authenticated())
//				.build();
//	}
//
//	@Bean
//	public UserDetailsService userDetailsService() {
//		return userDetailsService;
//	}
//
//}
