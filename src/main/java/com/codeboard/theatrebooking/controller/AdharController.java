package com.codeboard.theatrebooking.controller;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.codeboard.theatrebooking.model.UserAdhar;
import com.codeboard.theatrebooking.service.AdharService;


@RestController
public class AdharController {

	@Autowired
	private AdharService adharService;

	@PostMapping("/adhar")
	public int addAdhar(@RequestParam("name") String name,@RequestParam("adhar") MultipartFile adhar, Model model) throws Exception {
		int id = adharService.addAdhar(name, adhar);
		return id;
	}
	
	@GetMapping("/adhar/{id}")
	public String getPhoto(@PathVariable int id, Model model) {
	    UserAdhar adhar = adharService.getAdhar(id);
	    model.addAttribute("name", adhar.getName());
	    model.addAttribute("adhar",Base64.getEncoder().encodeToString(adhar.getFile().getData()));
	    return "Adhar"; 

}
}