package com.codeboard.theatrebooking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.codeboard.theatrebooking.model.Films;
import com.codeboard.theatrebooking.repository.FilmsRepository;
import com.codeboard.theatrebooking.service.FilmsService;

@RestController
@CrossOrigin("http://localhost:4200/")
public class FilmsController {
	
	@Autowired
	private FilmsService filmsService;
	
//	@Autowired
//	private FilmsRepository filmsRepository;
	
	@GetMapping("/films")
	public List<Films> getFilms() {
		return filmsService.getFilms();
	}
	
	@GetMapping("/films/{id}")
	public ResponseEntity<Films>   getFilms(@PathVariable("id") int id) {
		return filmsService.getFilmById(id);
	}
}
