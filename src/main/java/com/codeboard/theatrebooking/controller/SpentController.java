package com.codeboard.theatrebooking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codeboard.theatrebooking.model.Spent;
import com.codeboard.theatrebooking.repository.SpentRepository;
import com.codeboard.theatrebooking.service.SpentService;

@RestController
@CrossOrigin("http://localhost:4200/")
public class SpentController {
	
	@Autowired
	private SpentService spentService;
	
	@GetMapping("/spent")
	public List<Spent> getSpent(){
		return spentService.getSpent();
	}
	
	@PostMapping("/spent")
	public Spent postSpent(@RequestBody Spent spent) {
		return spentService.postSpent(spent);
	}
	

}
