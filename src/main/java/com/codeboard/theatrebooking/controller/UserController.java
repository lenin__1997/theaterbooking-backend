package com.codeboard.theatrebooking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codeboard.theatrebooking.model.BookingDetails;
import com.codeboard.theatrebooking.model.User;
import com.codeboard.theatrebooking.service.UserService;

@RestController
@CrossOrigin("http://localhost:4200/")
public class UserController {

	@Autowired
	private UserService userService;

//	@Autowired
//	private EmailSenderService emailSenderService;

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@GetMapping("/user")
	public List<User> getUser() {
		return userService.getUser();
	}

	@PostMapping("/user")
	public ResponseEntity<User> postUser(@RequestBody User user) {
		return userService.postUser(user);
	}

	@GetMapping("/userlogin/{username}/{password}")
	public User Login(@PathVariable("username") String username, @PathVariable("password") String password) {

		return userService.Login(username, password);
	}

	@PostMapping("/buy")
//		@EventListener(ApplicationReadyEvent.class)
	public void buyTicket(@RequestBody BookingDetails BookingDetails) {
//		SimpleMailMessage message = new SimpleMailMessage();
//		message.setTo();
//		message.setText();
//		userService.sendEmail(bookingDetails.getEmail(), bookingDetails.getMovie());
	}

	@PostMapping("/hello")
	public String publish(@RequestBody BookingDetails BookingDetails) {
		kafkaTemplate.send("helloTopic", BookingDetails.getEmail());
		return "Message Published : " + BookingDetails.getEmail();
	}

}
