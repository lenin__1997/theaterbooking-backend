package com.codeboard.theatrebooking.model;

public class BookingDetails {

	private String movie;
	private String email;

	public String getMovie() {
		return movie;
	}

	public void setMovie(String movie) {
		this.movie = movie;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
