package com.codeboard.theatrebooking.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "users")
public class User 
{

	/**
	implements
	UserDetails 
	 * 
	 */
//	private static final long serialVersionUID = -3737812268559572338L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "email")
	private String email;

	@Column(name = "number")
	private String number;

	@Column(name = "adhar")
	private String adhar;

	public User() {
		super();
	}

	public User(int id, String user, String password, String email, String number, String adhar) {
		super();
		this.id = id;
		this.username = user;
		this.password = password;
		this.email = email;
		this.number = number;
		this.adhar = adhar;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return username;
	}

	public void setUser(String user) {
		this.username = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getAdhar() {
		return adhar;
	}

	public void setAdhar(String adhar) {
		this.adhar = adhar;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", user=" + username + ", password=" + password + ", email=" + email + ", number="
				+ number + ", adhar=" + adhar + "]";
	}

}
