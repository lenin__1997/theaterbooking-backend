package com.codeboard.theatrebooking.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.codeboard.theatrebooking.model.UserAdhar;

@Repository
public interface AdharRepository extends MongoRepository<UserAdhar, Integer> {

}
