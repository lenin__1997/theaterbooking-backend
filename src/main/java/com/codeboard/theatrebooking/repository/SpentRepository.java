package com.codeboard.theatrebooking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codeboard.theatrebooking.model.Spent;

@Repository
public interface SpentRepository extends JpaRepository<Spent, Integer> {



}
