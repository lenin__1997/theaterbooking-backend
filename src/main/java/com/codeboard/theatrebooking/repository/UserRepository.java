package com.codeboard.theatrebooking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codeboard.theatrebooking.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	User getByUsername(String username);

}
