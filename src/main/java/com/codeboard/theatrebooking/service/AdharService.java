package com.codeboard.theatrebooking.service;

import org.springframework.web.multipart.MultipartFile;

import com.codeboard.theatrebooking.model.UserAdhar;

public interface AdharService {

	int addAdhar(String name, MultipartFile file) throws Exception;

	UserAdhar getAdhar(int id);

}
