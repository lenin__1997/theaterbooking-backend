package com.codeboard.theatrebooking.service;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.codeboard.theatrebooking.model.UserAdhar;
import com.codeboard.theatrebooking.repository.AdharRepository;

@Service
public class AdharServiceImple implements AdharService {
	
	@Autowired
	private AdharRepository adharRepository;
	
	@Override
	public int addAdhar(String name,MultipartFile file) throws Exception {
		UserAdhar adhar = new UserAdhar();
		adhar.setFile(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
		adhar = adharRepository.insert(adhar);
		return adhar.getId();
	}
	
	@Override
	public UserAdhar getAdhar(int id) {
		return adharRepository.findById(id).get();
	}
}
