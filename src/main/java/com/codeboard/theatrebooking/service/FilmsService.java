package com.codeboard.theatrebooking.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.codeboard.theatrebooking.model.Films;
import com.codeboard.theatrebooking.model.Spent;

public interface FilmsService {

	List<Films> getFilms();


	ResponseEntity<Films> getFilmById(int id);
	

}
