package com.codeboard.theatrebooking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.codeboard.theatrebooking.model.Films;
import com.codeboard.theatrebooking.model.Spent;
import com.codeboard.theatrebooking.repository.FilmsRepository;

@Service
public class FilmsServiceImple implements FilmsService {

	@Autowired
	private FilmsRepository filmsRepository;

	@Override
	public List<Films> getFilms() {
		return filmsRepository.findAll();
	}
	
	@Override
	public ResponseEntity<Films>  getFilmById( int id) {
		Films film= filmsRepository.getReferenceById(id);
		return new ResponseEntity<Films>(film,HttpStatus.OK);
	}


	
}
