package com.codeboard.theatrebooking.service;

import java.util.List;

import com.codeboard.theatrebooking.model.Spent;

public interface SpentService {
	
	List<Spent> getSpent();

	Spent postSpent(Spent spent);

	
}
