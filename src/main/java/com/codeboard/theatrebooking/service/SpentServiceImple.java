package com.codeboard.theatrebooking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codeboard.theatrebooking.model.Spent;
import com.codeboard.theatrebooking.repository.SpentRepository;

@Service
public class SpentServiceImple implements SpentService {
	
	@Autowired
	private SpentRepository spentRepository;
	
	@Override
	public List<Spent> getSpent(){
		return spentRepository.findAll();
	}

	@Override
	public Spent postSpent(Spent spent) {
		return spentRepository.save(spent);
	}
	

}
