package com.codeboard.theatrebooking.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.codeboard.theatrebooking.model.User;

public interface UserService
//extends UserDetailsService 
{

	List<User> getUser();

	ResponseEntity<User> postUser(User user);

//	String getUserEmail(String user);

	User Login(String user, String password);

//	void sendEmail(String email, String movieName);

}
