package com.codeboard.theatrebooking.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.codeboard.theatrebooking.model.User;
import com.codeboard.theatrebooking.repository.UserRepository;

@Service
public class UserServiceImple implements UserService {

	@Autowired
	private UserRepository userRepository;

//	@Autowired
//	private KafkaConsumer<String, String> kafkaConsumer;

//	@Autowired
//	private JavaMailSender javaMailSender;

	@Override
	public List<User> getUser() {
		return userRepository.findAll();
	}

//	@Override
//	public String getUserEmail(String user) {
//		User user1 = userRepository.getByUser(user);
//		return user1.getEmail();
//	}

	@Override
	public ResponseEntity<User> postUser(User user) {
		User user1 = userRepository.save(user);
		return ResponseEntity.ok(user1);
	}

//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		return userRepository.getByUsername(username);
//	}

	@Override
	public User Login(String name, String password) {
		User user = userRepository.getByUsername(name);
		if (Objects.nonNull(user)) {
			if (name.equals(user.getUser()) && password.equals(user.getPassword())) {
				return user;
			}
		}
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username or password");
	}
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate;
	
//	@GetMapping("/hello/{message}")
//	public String publish(@PathVariable("message")String publishMessage) {
//		kafkaTemplate.send("helloTopic",publishMessage);
//		return "Message Published : " + publishMessage;
//	}
	
//	@Override
//	public void sendEmail(String email, String movieName) {
//		kafkaConsumer.(email, movieName);
//
//	}

}
